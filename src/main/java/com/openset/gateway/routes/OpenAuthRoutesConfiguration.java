package com.openset.gateway.routes;


import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder.Builder;


@Configuration
public class OpenAuthRoutesConfiguration {
	
	@Value("${openauth.uri}")
	private String openauthUri;
	
	@Value("${openauth.paths}")
	private String openauthPath; 
	
	@Value("${openserver.uri}")
	private String openserverUri;
	
	@Value("${openserver.paths}")
	private String openserverPath; 
	
	
	
	@Bean("openauth")
	public RouteLocator OpenAuthRouteLocator(RouteLocatorBuilder builder) {
		Builder build = builder.routes(); 
		openAuthPaths().stream().forEach(
				path -> build.route(p -> p.path(path).uri(openauthUri))
				);
		return build.build(); 
	}

	@Bean("openserver")
	public RouteLocator ServerRouteLocator(RouteLocatorBuilder builder) {
		Builder routeBuilder = builder.routes(); 
		openServerPaths().stream().forEach(
				path -> routeBuilder.route(p -> p.path(path).uri(openserverUri))
				);
		return routeBuilder.build(); 
	}
	
	
	private List<String> openAuthPaths() {
		return List.of(openauthPath.split(",")); 
	}
	
	private List<String> openServerPaths() {
		return List.of(openserverPath.split(",")); 
	}
	
}
