package com.openset.gateway;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
class GatewayApplicationTests {
	
	@Autowired
	WebTestClient webClient; 
	
	@Test
	void badRequest_authenticationPath() {
		webClient.post().uri("/authentication").exchange().expectStatus().isBadRequest();
	}
	
	@Test
	void badRequest_registerPath() {
		webClient.post().uri("/register").exchange().expectStatus().isBadRequest();
	}
	
	@Test
	void badRequest_validatePath() {
		webClient.post().uri("/register").exchange().expectStatus().isBadRequest();
	}
	
	@Test
	void badRequest_notAllowed() {
		webClient.post().uri("/api/query").exchange().expectStatus().is4xxClientError();
	}
	
}
